<!DOCTYPE html>
<?php //require_once("modules/classes/all.php"); ?>
<?php 
    $title = "Sve kategorije"; 
    $description = "";
    $company_name = "";
    $keywords = "";
    $strana = ''; 

?>
<html>
    <head>
        @include('partials/head')

        @if(Session::has('b2c_admin'.B2bOptions::server()))
            <link href="{{ B2bOptions::base_url()}}css/ui.colorpicker.css" rel="stylesheet" type="text/css" />
            {{B2bOptions::css_prev()}}
        @endif
        
        <script type="application/ld+json">
            {  
                "@context" : "http://schema.org",
                "@type" : "WebSite",
                "name" : "<?php echo B2bOptions::company_name(); ?>",
                "alternateName" : "<?php echo $title." | ".B2bOptions::company_name(); ?>",
                "url" : "<?php echo B2bOptions::base_url(); ?>"
            }
        </script>

    </head>
<body>
    <div id="main-wrapper">
        <!-- PREHEADER -->
        @include('menu_top')
        <!-- HEADER -->
        <header class="row">
            
            <!-- LOGO AREA -->
            <section class="logo-area medium-3 columns"><a class="logo" href="{{ B2bOptions::base_url()}}" title="{{B2bOptions::company_name()}}"><img src="{{ B2bOptions::base_url()}}{{B2bOptions::company_logo()}}" alt="{{B2bOptions::company_name()}}" /></a></section>
            
            <!-- SEARCH AREA -->
            <section class="medium-6 columns left">
                <div class="search">
                    <input class="search-field" type="text" id="search" placeholder="Pretraga" />
                    <button onclick="search()" class="search-button"> <i class="fa fa-search"></i> </button>
                </div>
            </section>
            
            <!-- CART AREA -->
            @include('cart_top')
            
            
        </header>
        
        <!-- MIDDLE AREA -->
        <section id="middle-area" class="row">
            

            
            <!-- MAIN CONTENT -->
            <section id="main-content" class="medium-9 columns">
                <?php 
                    $query_category_first=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>0,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc')->get(); 
                ?>

                @foreach ($query_category_first as $row1)
                    @if(B2bCommon::broj_cerki($row1->grupa_pr_id) >0)
                        <!-- ROW 1 -->
                       <div class="row">
                            <h2 id="{{ $row1->grupa_pr_id  }}" class="category-heading">{{ $row1->grupa }} <span class="category__list__item__link__span">({{B2bCommon::brojiArtikleRekurzivno($row1->grupa_pr_id)}})</span></h2>



                            <?php $query_category_second=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row1->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('grupa_pr_id','asc') ?>
                            
                            @foreach ($query_category_second->get() as $row2)
                               <div class="columns medium-4">
                                    <a class="category-name-link" href="{{ B2bOptions::base_url()}}b2b/artikli/{{ B2bUrl::url_convert($row1->grupa) }}/{{ B2bUrl::url_convert($row2->grupa) }}">{{ $row2->grupa }} <span class="category__list__item__link__span">({{B2bCommon::brojiArtikleRekurzivno($row2->grupa_pr_id)}})</span></a>
                                    
                                    <ul class="category__list">
                                        <?php $query_category_third=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row2->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('grupa_pr_id','asc')->get(); ?>
                                        @foreach($query_category_third as $row3)
                                        <li class="category__list__item">
                                            <a class="category__list__item__link" href="{{ B2bOptions::base_url()}}b2b/artikli/{{ B2bUrl::url_convert($row1->grupa) }}/{{ B2bUrl::url_convert($row2->grupa) }}/{{ B2bUrl::url_convert($row3->grupa) }}">{{ $row3->grupa }} 
                                                <span class="category__list__item__link__span">({{B2bCommon::brojiArtikleRekurzivno($row3->grupa_pr_id)}})</span>
                                            </a>
                                        </li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endforeach

                       </div> <!-- end .row -->
                        <!-- END ROW 1 -->
                    @else
                        <div class="row">
                            <h2 id="{{ $row1->grupa_pr_id  }}" class="category-heading">{{ $row1->grupa  }} <span class="category__list__item__link__span">({{B2bCommon::brojiArtikleRekurzivno($row1->grupa_pr_id)}})</span></h2>

                            <?php $query_category_second=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row1->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('grupa_pr_id','asc') ?>
                            
                            @foreach ($query_category_second->get() as $row2)
                               <div class="columns medium-4">
                                    <a class="category-name-link" href="{{ B2bOptions::base_url()}}b2b/artikli/{{ B2bUrl::url_convert($row1->grupa) }}/{{ B2bUrl::url_convert($row2->grupa) }}">{{ $row2->grupa }} <span class="category__list__item__link__span">({{B2bCommon::brojiArtikleRekurzivno($row2->grupa_pr_id)}})</span></a>
                                    
                                    <ul class="category__list">
                                        <?php $query_category_third=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row2->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('grupa_pr_id','asc')->get(); ?>
                                        @foreach($query_category_third as $row3)
                                        <li class="category__list__item">
                                            <a class="category__list__item__link" href="{{ B2bOptions::base_url()}}b2b/artikli/{{ B2bUrl::url_convert($row1->grupa) }}/{{ B2bUrl::url_convert($row2->grupa) }}/{{ B2bUrl::url_convert($row3->grupa) }}">{{ $row3->grupa }} 
                                                <span class="category__list__item__link__span">({{B2bCommon::brojiArtikleRekurzivno($row3->grupa_pr_id)}})</span>
                                            </a>
                                        </li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endforeach

                       </div> <!-- end .row -->                        
                    
                    @endif
                @endforeach 
                
            </section><!-- end #main-content -->

             <!-- MAIN CONTENT -->
            <section id="category-sidebar" class="medium-3 columns">
                
                <!-- caregories HERE -->
                
                <ul class="category-sidebar__list " >
                    <div class="category-sidebar__list__toggler"></div>
                    
                    @foreach ($query_category_first as $row1)
                    <li class="category-sidebar__list__item">
                        <a href="#{{ $row1->grupa_pr_id  }}" class="category-sidebar__list__item__link">{{ $row1->grupa  }} <span class="category-sidebar__list__item__link__span">({{B2bCommon::brojiArtikleRekurzivno($row1->grupa_pr_id)}})</span></a>
                    </li>
                    @endforeach 
                </ul>

            </section>

            
        </section>
        
        <!-- FOOTER -->
        @include('footer')
        
    </div>
    <!-- LOGIN POPUP -->
    @include('login_registracija')
    <script src="{{ B2bOptions::base_url()}}js/jquery-1.11.2.min.js" type="text/javascript" ></script>
    @if(Session::has('b2c_admin'.B2bOptions::server()))
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    @endif
    @if($strana == B2bCommon::get_page_start())
    <script src="{{ B2bOptions::base_url()}}js/slick.min.js" type="text/javascript" ></script>
    @endif
    <script src="{{ B2bOptions::base_url()}}js/b2b/main_function.js" type="text/javascript" ></script>
    <script src="{{ B2bOptions::base_url()}}js/b2b/b2b_main.js" type="text/javascript" ></script>
    @if(B2bOptions::header_type()==1)
        <!-- fixed navgation bar -->
        <script src="{{ B2bOptions::base_url() }}/js/b2b/fixed_header.js"></script>
    @endif
    <script src="{{ B2bOptions::base_url()}}js/b2b/all-categories.js" type="text/javascript" ></script>
    <script src="{{ B2bOptions::base_url()}}js/jquery.fancybox.pack.js" type="text/javascript" ></script>
    <?php if($strana == B2bCommon::get_kontakt()){?>
    
    <script src="https://maps.googleapis.com/maps/api/js"></script>
    <script>
        function initialize() {
            var map_canvas = document.getElementById('map_canvas');
            var map_options = {
                center: {lat: 43.315366, lng:  21.874909},
                zoom: 17,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            }
            var map = new google.maps.Map(map_canvas, map_options);
            var marker = new google.maps.Marker({
                position: {lat: 43.315366, lng:  21.874909},
                map: map,
                title: 'Nasa lokacija'
            });
            
        } // end initialize()
        google.maps.event.addDomListener(window, 'load', initialize);
    </script>
    <?php }?>
    
    
</body>
</html>