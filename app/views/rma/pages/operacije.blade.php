@extends('rma.templates.main')
@section('content')
<section id="main-content">

	<div class="row">
		<div class="small-12 columns"> 
			<a href="{{ RmaOptions::base_url() }}rma/operacije-export" class="btn btn-small btn-primary">Eksport svih operacija</a>
		</div>
	</div>
	<div class="row">
		
		<section class="small-12 medium-12 large-6 columns">
			<div class="flat-box">
						
				<h3 class="title-med">Grupe</h3>
				<div class="row nova-karak">
					<div class="columns medium-7">Naziv</div>
				</div>

				<ul class="banner-list name-ul">
					<li class="nova-karak_li name-ul__li">
						<div class="columns medium-3">Nova grupa:</div>
						<div class="columns medium-9">
							<input class="JSOperacijaGrupa naziv name-ul__li__name" name="naziv" type="text" value="" data-id="0" autocomplete="off" {{ Admin_model::check_admin(array('RMA_AZURIRANJE')) == false ? 'readonly' : '' }}>
						</div>
					</li>
				
				@foreach($grupe as $grupa)
					<li class="name-ul__li @if($grupa->operacija_grupa_id == $grupa_id) active @endif" id="{{ $grupa->operacija_grupa_id }}" >
						<div class="columns medium-3">&nbsp;</div>
						<div class="columns medium-7">
							<input class="JSOperacijaGrupa naziv name-ul__li__name" name="naziv" type="text" value="{{ htmlentities($grupa->naziv) }}" data-id="{{ $grupa->operacija_grupa_id }}" autocomplete="off" {{ Admin_model::check_admin(array('RMA_AZURIRANJE')) == false ? 'readonly' : '' }}>
						</div>
						<div class="columns medium-2 text-right"> 
							@if(Admin_model::check_admin(array('RMA_AZURIRANJE'))) 
								<button title="Obriši" class="JSbtn-delete obrisi-karak name-ul__li__remove button-option tooltipz" aria-label="Obriši" data-link="{{ RmaOptions::base_url() }}rma/operacija-grupa/{{ $grupa->operacija_grupa_id }}/delete"><i class="fa fa-times" aria-hidden="true"></i></button>
							@endif
							
							<a href="{{ RmaOptions::base_url() }}rma/operacije/{{ $grupa->operacija_grupa_id }}" class="name-ul__li__right-arrow button-option tooltipz" aria-label="Dodaj vrednost">
								<i class="fa fa-arrow-right" aria-hidden="true"></i>
							</a>
						</div> 
					</li>
				@endforeach
				</ul>
 			</div> <!-- end of .flat-box -->
 			
		</section>
	
		@if($grupa_id > 0)
		<section class="small-12 medium-6 columns">
			<div class="flat-box">
						
				<h3 class="title-med">Operacije</h3>
				<div class="row nova-karak">
					<div class="columns medium-7">Naziv</div>					
				</div>

				<ul class="banner-list name-ul">
					<li class="nova-karak_li name-ul__li" data-id="{{ $grupa_id }}">
						<div class="columns medium-6"> 
							<input class="JSOperacija naziv name-ul__li__name" type="text" value="" data-id="0" autocomplete="off" {{ Admin_model::check_admin(array('RMA_AZURIRANJE')) == false ? 'readonly' : '' }}>
						</div>
						<div class="columns medium-5"> 
							<input class="JSOperacijaCena naziv name-ul__li__name" type="text" value="0.00" data-id="0" autocomplete="off" {{ Admin_model::check_admin(array('RMA_AZURIRANJE')) == false ? 'readonly' : '' }}>
						</div>
						<div class="columns medium-1">&nbsp;</div>
					</li>

				@foreach($operacije as $operacija)
					<li class="name-ul__li @if($operacija->operacija_id == $id) active @endif" id="{{ $operacija->operacija_grupa_id }}" >
						<div class="columns medium-6"> 
							<input class="JSOperacija naziv name-ul__li__name" type="text" value="{{ htmlentities($operacija->naziv) }}" data-id="{{ $operacija->operacija_id }}" autocomplete="off" {{ Admin_model::check_admin(array('RMA_AZURIRANJE')) == false ? 'readonly' : '' }}>
						</div>
						<div class="columns medium-5"> 
							<input class="JSOperacijaCena naziv name-ul__li__name" type="text" value="{{ htmlentities($operacija->cena) }}" data-id="{{ $operacija->operacija_id }}" autocomplete="off" {{ Admin_model::check_admin(array('RMA_AZURIRANJE')) == false ? 'readonly' : '' }}>
						</div>
						<div class="columns medium-1"> 
							@if(Admin_model::check_admin(array('RMA_AZURIRANJE')))
								<button title="Obriši" class="JSbtn-delete obrisi-karak name-ul__li__remove button-option tooltipz" aria-label="Obriši" data-link="{{ RmaOptions::base_url() }}rma/operacija/{{ $operacija->operacija_id }}/delete"><i class="fa fa-times" aria-hidden="true"></i></button>
							@endif
						</div>
			<!-- 			<a href="{{ RmaOptions::base_url() }}rma/operacije/{{ $operacija->operacija_grupa_id }}/{{ $operacija->operacija_id }}" class="name-ul__li__right-arrow button-option tooltipz" aria-label="Dodaj vrednost">
							<i class="fa fa-arrow-right" aria-hidden="true"></i>
						</a> -->
					</li>
				@endforeach
				</ul>
 			</div> <!-- end of .flat-box -->
 			
		</section>
		@endif

<!-- 		@if($id > 0)
		<section class="small-12 medium-12 large-4 columns">
			<div class="flat-box">
						
				<h3 class="title-med">Operacije Servisa - {{ $operacija_naziv }}</h3>
				<div class="row nova-karak">
					<div class="columns medium-5">Servis</div>
					<div class="columns medium-5">Cena</div>
				</div>

				<ul class="banner-list name-ul">
					<li class="nova-karak_li name-ul__li">
						<select id="JSServisId">
							{{ RMA::servisSelect() }}
						</select>
						 <input class="JSOperacijaServisa naziv name-ul__li__name input-smaller" name="naziv" type="text" value="" data-operacija_servis_id="0" data-operacija_grupa_id="{{ $grupa_id }}" data-operacija_id="{{ $id }}" autocomplete="off">
					</li>
				
				@foreach($operacije_servisa as $operacija_servis)
					<li class="name-ul__li" id="{{ $operacija_servis->operacija_servis_id }}" >
						<div class="JSOperacija naziv name-ul__li__name input-smaller">{{ RMA::partner($operacija_servis->partner_id)->naziv }}</div>

						 <input class="JSOperacijaServisa naziv name-ul__li__name input-smaller" name="naziv" type="text" value="{{ htmlentities($operacija_servis->cena) }}" data-operacija_servis_id="{{ $operacija_servis->operacija_servis_id }}" data-operacija_grupa_id="{{ $grupa_id }}" data-operacija_id="{{ $id }}" data-partner_id="{{ $operacija_servis->partner_id }}" autocomplete="off">

						<button title="Obriši" class="JSbtn-delete obrisi-karak name-ul__li__remove button-option tooltipz" aria-label="Obriši" data-link="{{ RmaOptions::base_url() }}rma/operacija-servis/{{ $operacija_servis->operacija_servis_id }}/delete"><i class="fa fa-times" aria-hidden="true"></i></button>
						
					</li>
				@endforeach
				</ul>
 			</div> 
 			
		</section>
		@endif -->
	</div>

</section>
@endsection