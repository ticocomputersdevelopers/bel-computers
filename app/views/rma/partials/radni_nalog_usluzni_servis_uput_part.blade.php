<div class="container relative"> 
	<div class="row header">
		<div class="col-3">
			<img class="logo" src="{{ Options::base_url()}}{{Options::company_logo()}}" alt="logo">
		</div>
		<div class="col-4"> 
			<p>Firma: {{RmaOptions::company_name()}}</p>
			<p>Adresa: {{RmaOptions::company_adress()}}</p>
			<p>Telefon: {{RmaOptions::company_phone()}}</p>
			<p>Fax: {{RmaOptions::company_fax()}}</p>
			<p>PIB: {{RmaOptions::company_pib()}}</p>
			<p>E-mail: {{RmaOptions::company_email()}}</p> 
		</div>
		<div class="col-4"> 
			<p>Servis: {{$usluzni_servis->naziv}}</p>
			<p>Adresa: {{$usluzni_servis->adresa.', '.$usluzni_servis->mesto}}</p>
			<p>Telefon: {{$usluzni_servis->telefon}}</p>
			<p>Fax: {{$usluzni_servis->fax}}</p>
			<p>PIB: {{$usluzni_servis->pib}}</p>
			<p>E-mail: {{$usluzni_servis->mail}}</p> 
		</div>
	</div>

	<div class="main-content"> 
		<div class="row">
			<div class="col-12">  
				<p>Uput na servis br: {{ $radni_nalog_trosak_uput->radni_nalog_trosak_broj_dokumenta }}</p>
				<p>Datum: {{ RMA::format_datuma($radni_nalog_trosak_uput->datum_dokumenta) }}</p>  
			</div>
		</div>
		 
		<div class="row"><br>
			<div class="col-2"><p>Uređaj:</p></div>
			<div class="col-9 border-btm"><p>{{ $radni_nalog_trosak_uput->uredjaj }}&nbsp;</p></div>  
		</div>

		<div class="row">
			<div class="col-2"><p>Serijski broj:</p></div>
			<div class="col-9 border-btm"><p>{{ $radni_nalog_trosak_uput->serijski_broj }}&nbsp;</p></div>
		</div>

		<div class="row"><br><br>  
			<div class="col-2"><p>Opis Kvara:</p></div>
			<div class="col-9 border-btm"><p>{{ $radni_nalog_trosak_uput->opis_kvara }}&nbsp;</p></div> 
		</div>
		<div class="row"><div class="col-11 border-btm"><p>&nbsp;</p></div></div>

		<div class="row"><br><br> 
			<div class="col-2"><p>Napomena:</p></div>
			<div class="col-9 border-btm"><p>{{ $radni_nalog_trosak_uput->napomena }}&nbsp;</p></div> 
		</div>
		<div class="row"><div class="col-11 border-btm"><p>&nbsp;</p></div></div>
 
		<div class="row text-center"><br><br>
			<div class="col-6">
				<p>Predao</p><br>
				<p>___________________________MP</p>
			</div>
			<div class="col-5">
				<p>Primio</p><br>
				<p>___________________________MP</p>
			</div>
		</div> 
	</div>
</div> 