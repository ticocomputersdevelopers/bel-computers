<div id="main-content">
	@include('admin/partials/product-tabs')
	@if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
	<div class="row">
		<div class="columns medium-6 medium-centered">
			
			<br>

			<div class="relative">
				<input class="" autocomplete="off" data-timer="" type="text" id="articles" data-id="{{ $roba_id }}" placeholder="{{ AdminLanguage::transAdmin('Unesite id ili ime artikla ili prozvođača ili krajnju grupu') }}" />
				
				<div class="flat-box"> 
					<div id="search_content"></div>
				</div>
			</div>
		</div>
	</div>
	@endif

	<div class="row"> 
		<div class="columns medium-6 medium-centered">
			<div class="flat-box">
				<div class="table-scroll">
					<table>
						@if(count($vezani_artikli)>0)
						<thead>
							<tr>
								<th>{{ AdminLanguage::transAdmin('ROBA ID') }}</th>
								<th>{{ AdminLanguage::transAdmin('GRUPA') }}</th>
								<th>{{ AdminLanguage::transAdmin('NAZIV') }}</th>
								<th colspan="4">{{ AdminLanguage::transAdmin('FLAG CENE') }}</th> 
							</tr>
						</thead>

						<tbody> 
							@foreach($vezani_artikli as $row)
							<tr>
								<td>&nbsp;{{ $row->vezani_roba_id }}</td>
								<td>{{ AdminArticles::findGrupe(AdminArticles::find($row->vezani_roba_id,'grupa_pr_id'),'grupa') }}</td>

								<td>
									<div class="max-width-450 no-white-space"> 
										{{ AdminArticles::find($row->vezani_roba_id,'naziv') }}
									</div>
								</td>

								<td>
									<span class="tooltipz inline-block" aria-label="{{ AdminArticles::find($row->vezani_roba_id,'web_cena') }}">
										<input type="number" onkeydown="javascript: return event.keyCode === 8 || event.keyCode === 46 || event.keyCode === 190 || event.keyCode === 37 || event.keyCode === 39 ? true : !isNaN(Number(event.key))" class="JSCenaVezan" value="{{$row->cena}}" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'readonly' : '' }}>
									</span>
								</td>
								<td>
									@if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
									<button class="JSsacuvajCenu btn-secondary btn btn-small" data-store='{"roba_id":"{{ $roba_id }}", "vezani_roba_id":"{{ $row->vezani_roba_id }}"}'>{{ AdminLanguage::transAdmin('Sačuvaj') }}</button>
									@endif
								</td>
								<td>
									@if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
									<span class="tooltipz inline-block" aria-label="Prikaži korpu">
										<input type="checkbox" class="JSFlagCenaVezan" data-store='{"roba_id":"{{ $roba_id }}", "vezani_roba_id":"{{ $row->vezani_roba_id }}"}' {{ $row->flag_cena == 1 ? 'checked' : '' }}>
									</span>
									@endif
								</td>
								<td>
									@if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
									<button class="JSDeleteVezan btn btn-xm btn-secondary" data-store='{"roba_id":"{{ $roba_id }}", "vezani_roba_id":"{{ $row->vezani_roba_id }}"}'><i class="fa fa-times"></i></button>
									@endif
								</td>
							</tr>
							@endforeach
						</tbody>
						@endif
					</table>
				</div>
			</div>
		</div>
	</div>
</div> 
