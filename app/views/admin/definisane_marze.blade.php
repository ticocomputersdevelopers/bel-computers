<section class="osobine-page" id="main-content">

	@include('admin/partials/tabs')
	<?php
		$messages = array();
		if($errors->first('nc')){ $messages[] = $errors->first('nc'); }
		if($errors->first('web_marza')){ $messages[] = $errors->first('web_marza'); }
		if($errors->first('mp_marza')){ $messages[] = $errors->first('mp_marza'); }
	?>
	@if(count($messages)>0)
		<script>
			alertify.error('{{ $messages[0] }}');
		</script>
	@elseif(Session::has('success'))
		<script>
			alertify.success('{{ AdminLanguage::transAdmin('Uspešno ste sačuvali podatak') }}');
		</script>
	@elseif(Session::has('success-delete'))
		<script>
			alertify.success('{{ AdminLanguage::transAdmin('Uspešno ste obrisali vrednost.') }}');
		</script>
	@endif
	
	<div class="row">
		<section class="small-12 medium-6 medium-centered columns">
			<div class="flat-box">
				<div class="table-scroll">
					<table class="def-marz">
						<thead>
							<tr>
								<th>NC(RSD) {{ AdminLanguage::transAdmin('od') }}</th>
								<th>NC(RSD) {{ AdminLanguage::transAdmin('do') }}</th>
								<th>{{ AdminLanguage::transAdmin('Veb marža') }}(%)</th>
								<th>{{ AdminLanguage::transAdmin('Mp. marža') }}(%)</th>
								@if(Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')))
									<th></th>
									<th></th>
								@endif
							</tr>
						</thead>
						<tbody>
							@foreach($definisane_marze as $row)
							<tr>
								<form method="POST" action="{{AdminOptions::base_url()}}admin/definisane-marze-save">
									<input type="hidden" name="definisane_marze_id" value="{{ $row->definisane_marze_id }}">
									<input type="hidden" name="web_marza" value="{{ $row->web_marza }}">				
									<td><input type="text" value="{{ $row->nc_od }}" disabled="true"></td>
									<td><input type="text" name="nc" value="{{ $row->nc_do }}" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'readonly' : '' }}></td>
									<td><input type="text" name="web_marza" value="{{ $row->web_marza }}" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'readonly' : '' }}></td>
									<td><input type="text" name="mp_marza" value="{{ $row->mp_marza }}" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'readonly' : '' }}></td>
									@if(Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')))
										<td><input type="submit" value="{{ AdminLanguage::transAdmin('Sačuvaj') }}"></td>
										<th>@if($row->definisane_marze_id!=0)
										<a class="JSbtn-delete" data-link="{{AdminOptions::base_url()}}admin/definisane-marze-delete/{{$row->definisane_marze_id}}"><i class="fa fa-times" style="color:red;" aria-hidden="true"></i>&nbsp;</a>@endif</th>
									@endif
								</form>
							</tr>
							@endforeach					
						</tbody>
					</table>
				</div>
			</div>
		</section>
	</div>
</section>


