<section id="main-content">

	@include('admin/partials/tabs')

	<?php
	$messages = array();
	if($errors->first('max_bodovi')){ $messages[] = $errors->first('max_bodovi'); }
	if($errors->first('vazi_do')){ $messages[] = $errors->first('vazi_do'); }

	if($errors->first('web_cena')){ $messages[] = $errors->first('web_cena'); }
	if($errors->first('popust')){ $messages[] = $errors->first('popust'); }

	if($errors->first('web_cena')){ $messages[] = $errors->first('web_cena'); }
	if($errors->first('broj_bodova')){ $messages[] = $errors->first('broj_bodova'); }

	?>
	@if(count($messages)>0)
	<script>
		alertify.error('{{ $messages[0] }}');
	</script>
	@elseif(Session::has('success'))
	<script>
		alertify.success('{{ AdminLanguage::transAdmin('Uspešno ste sačuvali podatak') }}');
	</script>
	@elseif(Session::has('success-delete'))
	<script>
		alertify.success('{{ AdminLanguage::transAdmin('Uspešno ste obrisali vrednost.') }}');
	</script>
	@endif

	<div class="row">
		<section class="medium-6 medium-centered columns">
			<div class="flat-box">
				
				<h3 class="title-med">{{ AdminLanguage::transAdmin('Podešavanja') }}</h3>

				<div class="table-scroll">
					<table>
						<thead>  
							<tr>
								<th>{{ AdminLanguage::transAdmin('Opcija') }}</th>
								<th>{{ AdminLanguage::transAdmin('Vrednost') }}</th> 
								<th></th> 
							</tr>
						</thead>

						<tbody>
							@foreach($bodovi_podesavanja as $row)	
							<tr> 
								<form method="POST" action="{{AdminOptions::base_url()}}admin/bodovi-podesavanja-save">

									<input type="hidden" name="web_options_id" value="{{ $row->web_options_id }}">

									<td>{{ $row->naziv }}</td>
									<td>
										<input type="text" name="int_data" value="{{ $row->int_data }}" 
										{{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'readonly' : '' }}>
									</td>
									
									
									@if(Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')))

									<td><input class="btn btn-small btn-secondary" type="submit" value="{{ AdminLanguage::transAdmin('Sačuvaj') }}"></td>

									@endif
								</form> 
							</tr>
							@endforeach			
						</tbody>
					</table>
				</div>
			</div>
		</section>
	</div>

	<div class="row">
		<section class="medium-6 medium-centered columns">
			<div class="flat-box">
				
				<h3 class="title-med">{{ AdminLanguage::transAdmin('Ostvarivanje bodova') }}</h3>

				<div class="table-scroll">
					<table>
						<thead>  
							<tr>
								<th>{{ AdminLanguage::transAdmin('Cena proizvoda') }} {{ AdminLanguage::transAdmin('od') }}</th>
								<th>{{ AdminLanguage::transAdmin('Cena proizvoda') }} {{ AdminLanguage::transAdmin('do') }}</th>
								<th>{{ AdminLanguage::transAdmin('Broj bodova') }}</th>
								
								@if(Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')))
								<th></th>
								<th></th>
								@endif
							</tr>
						</thead>

						<tbody>
							@foreach($bodovi_ostvareni as $row)
							<tr>
								<form method="POST" action="{{AdminOptions::base_url()}}admin/bodovi-ostvareni-save">
									<input type="hidden" name="bodovi_ostvareni_id" value="{{ $row->bodovi_ostvareni_id }}">
									<input type="hidden" name="broj_bodova" value="{{ $row->broj_bodova }}">				
									<td><input type="text" value="{{ $row->web_cena_od }}" disabled="true"></td>
									<td><input type="text" name="web_cena" value="{{ $row->web_cena_do }}" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'readonly' : '' }}></td>
									<td><input type="text" name="broj_bodova" value="{{ $row->broj_bodova }}" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'readonly' : '' }}></td>
									
									@if(Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')))
									<td>
										<input class="btn btn-small btn-secondary" type="submit" value="{{ AdminLanguage::transAdmin('Sačuvaj') }}">
									</td>
									<td>@if($row->bodovi_ostvareni_id!=0)
										<a class="JSbtn-delete" data-link="{{AdminOptions::base_url()}}admin/bodovi-ostvareni-delete/{{$row->bodovi_ostvareni_id}}"><i class="fa fa-times" style="color:red;" aria-hidden="true"></i>&nbsp;</a>@endif</td>
										@endif
									</form>
								</tr>
								@endforeach					
							</tbody>
						</table>
					</div>
				</div>
			</section>
		</div>

		<div class="row">
			<section class="medium-6 medium-centered columns">
				<div class="flat-box">
					<h3 class="title-med">{{ AdminLanguage::transAdmin('Popust') }}</h3>
					<div class="table-scroll">
						<table>
							<thead>  
								<tr>
									<th>{{ AdminLanguage::transAdmin('Cena proizvoda') }} {{ AdminLanguage::transAdmin('od') }}</th>
									<th>{{ AdminLanguage::transAdmin('Cena proizvoda') }} {{ AdminLanguage::transAdmin('do') }}</th>
									<th>{{ AdminLanguage::transAdmin('Popust') }}(%)</th>

									@if(Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')))
									<th></th>
									<th></th>
									@endif
								</tr>
							</thead>

							<tbody>
								@foreach($bodovi_popust as $row)
								<tr>
									<form method="POST" action="{{AdminOptions::base_url()}}admin/bodovi-popust-save">
										<input type="hidden" name="bodovi_popust_id" value="{{ $row->bodovi_popust_id }}">
										<input type="hidden" name="popust" value="{{ $row->popust }}">				
										
										<td><input type="text" value="{{ $row->web_cena_od }}" disabled="true"></td>
										
										<td><input type="text" name="web_cena" value="{{ $row->web_cena_do }}" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'readonly' : '' }}></td>
										
										<td><input type="text" name="popust" value="{{ $row->popust }}" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'readonly' : '' }}></td>

										@if(Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')))
										<td>
											<input class="btn btn-small btn-secondary" type="submit" value="{{ AdminLanguage::transAdmin('Sačuvaj') }}">
										</td>
										<td>
											@if($row->bodovi_popust_id!=0)
											<a class="JSbtn-delete" data-link="{{AdminOptions::base_url()}}admin/bodovi-popust-delete/{{$row->bodovi_popust_id}}"><i class="fa fa-times" style="color:red;" aria-hidden="true"></i>&nbsp;</a>@endif
										</td>
										@endif
									</form>
								</tr>
								@endforeach					
							</tbody>
						</table>
					</div>
				</div>
			</section>
		</div> 
	</section>


