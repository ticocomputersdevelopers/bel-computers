<!DOCTYPE html>
<html>
<head> 
<title>500 Internal Server Error</title>
<style type="text/css">
* { box-sizing: border-box; }
body{  
	font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
    font-size: 100%;
    line-height: 1.42857143;
    color: #666;
    background-color: #f2f2f2;
    margin: 0;
    padding: 0;
    overflow: hidden;
}
.container {
    max-width: 1170px;
    width: 100%;
    height: 100vh;
    padding: 0 15px 0;
    margin: 0 auto 0; 
    text-align: center;
}
.container > div{
	position: relative;
    top: 25%;
    transform: translate(0, -25%);
    -webkit-transform: translate(0, -25%);
}
.container span{
	font-size: 5em;
}
.container h1{
    font-size: 1.3em;
    font-weight: normal;
}
</style>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>

<div class="container">
	<div> 
		<span class="fa fa-gears"></span> 
		<h1>Došlo je do greške.<br>
			Molimo Vas pokušajte ponovo kasnije. 
		</h1>
	</div>
</div>

</body>
</html>
