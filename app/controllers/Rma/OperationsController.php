<?php 

class OperationsController extends Controller{

	public function operacije($grupa_id=0,$id=0){

	  	  $data=array(
            "strana"=>'operacije',
            "title"=>$grupa_id != 0 ? Operations::grupa($grupa_id)->naziv : 'Radne operacije',
            "grupa_id" => $grupa_id,
            "id" => $id,
            "operacija_naziv" => $id > 0 ? DB::table('operacija')->where('operacija_id',$id)->pluck('naziv') : '',
            "grupe" => DB::table('operacija_grupa')->orderBy('naziv','asc')->get(),
            "operacije" => $grupa_id > 0 ? DB::table('operacija')->where('operacija_grupa_id',$grupa_id)->orderBy('naziv','asc')->get() : array(),
            "operacije_servisa" => $id > 0 ? DB::table('operacija_servis')->where('operacija_id',$id)->orderBy('partner_id','asc')->get() : array()
        );
         
            return View::make('rma/pages/operacije', $data);
	}

	public function operacija_grupa_save(){
		$data = Input::get();

        $response = array(
        	'success'=>true,
        	'message'=>'Uspešno ste sačuvali grupu.'
        );

		$messages = array(
            'required' => 'Niste popunili polje!',
            'regex' => 'Polje sadržati nedozvoljene karaktere!'
        );

        $validator_arr = array(
            'naziv' => 'required|regex:'.RmaSupport::regex().'|max:255'
        );
        
        $validator = Validator::make($data, $validator_arr, $messages);

		if ($validator->fails()) {
			$response['success'] = false;
			$response['message'] = $validator->messages()->first();
		}else{
			if($data['operacija_grupa_id'] == 0){
				$operacija_grupa_id = DB::table('operacija_grupa')->max('operacija_grupa_id') + 1;
				DB::table('operacija_grupa')->insert(array('operacija_grupa_id'=> $operacija_grupa_id, 'naziv'=>$data['naziv']));
				AdminSupport::saveLog('RMA_OPERACIJA_GRUPA_DODAJ', array(DB::table('operacija_grupa')->max('operacija_grupa_id')));
			}else{
				DB::table('operacija_grupa')->where('operacija_grupa_id',$data['operacija_grupa_id'])->update(array('naziv'=>$data['naziv']));
				AdminSupport::saveLog('RMA_OPERACIJA_GRUPA_IZMENI', array($data['operacija_grupa_id']));
			}
		} 	
		return json_encode($response);
	}

	public function operacija_save(){
		$data = Input::get();
		
        $response = array(
        	'success'=>true,
        	'message'=>'Uspešno ste sačuvali grupu.'
        );

		$messages = array(
            'required' => 'Niste popunili polje!',
            'regex' => 'Polje sadržati nedozvoljene karaktere!',
            'max' => 'Sadržaj polja je predugačak!',
            'digits_between' => 'Sadržaj polja je predugačak!',
            'numeric' => 'Polje sme da sadrži samo brojeve!',
        );

        $validator_arr = array(
            'naziv' => 'required|regex:'.RmaSupport::regex().'|max:255',
            'cena' => 'required|numeric|digits_between:0,10'
        );
        $validator = Validator::make($data, $validator_arr, $messages);

		if ($validator->fails()) {
			$response['success'] = false;
			$response['message'] = $validator->messages()->first();
		}else{        
			if($data['operacija_id'] == 0){
				$operacija_id = DB::table('operacija')->max('operacija_id') + 1;
				DB::table('operacija')->insert(array('operacija_id'=> $operacija_id, 'operacija_grupa_id'=> $data['operacija_grupa_id'], 'naziv'=>$data['naziv'],'cena'=>$data['cena']));

				DB::table('operacija_servis')->insert(
					array(
						'operacija_servis_id'=> DB::table('operacija_servis')->max('operacija_servis_id') + 1,
						'partner_id'=>1,
						'operacija_id'=>$operacija_id,
						'tarifna_grupa_id'=> 0,
						'cena'=>$data['cena']
					)
				);
				AdminSupport::saveLog('RMA_OPERACIJA_DODAJ', array(DB::table('operacija')->max('operacija_id')));
			}else{
				DB::table('operacija')->where('operacija_id',$data['operacija_id'])->update(array('naziv'=>$data['naziv'],'cena'=>$data['cena']));
				DB::table('operacija_servis')->where(array('partner_id'=>1, 'operacija_id'=>$data['operacija_id']))->update(array('cena'=>$data['cena']));	
				AdminSupport::saveLog('RMA_OPERACIJA_IZMENI', array($data['operacija_id']));		
			}
		}

		return json_encode($response);
	}


	public function operacija_grupa_delete($grupa_id){
		$operacije_ids = array_map('current',DB::table('operacija')->select('operacija_id')->where('operacija_grupa_id',$grupa_id)->get());
		$operacija_servis_ids = array_map('current',DB::table('operacija_servis')->select('operacija_servis_id')->whereIn('operacija_id',$operacije_ids)->get());
		$radni_nalog_operacije_count = DB::table('radni_nalog_operacije')->whereIn('operacija_servis_id',$operacija_servis_ids)->count();

		if($radni_nalog_operacije_count > 0){
			return Redirect::to(RmaOptions::base_url().'rma/operacije/0')->with('error_message','Grupa ne može biti obrisana, jer je vezana za radni nalog!');
		}
		AdminSupport::saveLog('RMA_OPERACIJA_GRUPA_OBRISI', array($grupa_id));
		DB::table('operacija_servis')->whereIn('operacija_id',$operacije_ids)->delete();
		DB::table('operacija')->where('operacija_grupa_id',$grupa_id)->delete();
		DB::table('operacija_grupa')->where('operacija_grupa_id',$grupa_id)->delete();

		return Redirect::to(RmaOptions::base_url().'rma/operacije/0')->with('message','Uspešno ste obrisali grupu.');
	}

	public function operacija_delete($operacija_id){
		$operacija = DB::table('operacija')->where('operacija_id',$operacija_id)->first();
		$operacija_servis_ids = array_map('current',DB::table('operacija_servis')->select('operacija_servis_id')->where('operacija_id',$operacija_id)->get());
		$radni_nalog_operacije_count = DB::table('radni_nalog_operacije')->whereIn('operacija_servis_id',$operacija_servis_ids)->count();
		if($radni_nalog_operacije_count > 0){
			return Redirect::to(RmaOptions::base_url().'rma/operacije/'.$operacija->operacija_grupa_id)->with('error_message','Operacija ne može biti obrisana, jer je vezana za radni nalog!');
		}
		AdminSupport::saveLog('RMA_OPERACIJA_OBRISI', array($operacija_id));
		DB::table('operacija_servis')->where('operacija_id',$operacija_id)->delete();
		DB::table('operacija')->where('operacija_id',$operacija_id)->delete();

		return Redirect::to(RmaOptions::base_url().'rma/operacije/'.$operacija->operacija_grupa_id)->with('message','Uspešno ste obrisali operaciju.');
	}

	public function operacija_sevis_delete($operacija_servis_id){
		$operacija = DB::table('operacija')->where('operacija_id',DB::table('operacija_servis')->where('operacija_servis_id',$operacija_servis_id)->pluck('operacija_id'))->first();

		$radni_nalog_operacije_count = DB::table('radni_nalog_operacije')->where('operacija_servis_id',$operacija_servis_id)->count();
		if($radni_nalog_operacije_count > 0){
			return Redirect::to(RmaOptions::base_url().'rma/operacije/'.$operacija->operacija_grupa_id.'/'.$operacija->operacija_id)->with('error_message','Operacija servisa ne može biti obrisana, jer je vezana za radni nalog!');
		}
		DB::table('operacija_servis')->where('operacija_servis_id',$operacija_servis_id)->delete();

		return Redirect::to(RmaOptions::base_url().'rma/operacije/'.$operacija->operacija_grupa_id.'/'.$operacija->operacija_id)->with('message','Uspešno ste obrisali operaciju servisa.');
	}

	public function operacija_servis_save(){
		$data = Input::get();

        $messages = array(
            'required' => 'Niste popunili polje!',
            'numeric' => 'Polje sme da sadrži samo brojeve!',
            'integer' => 'Vrednost sadržaja nije dozvoljena!',
            'digits_between' => 'Dužina sadržaja nije odgovarajuća'
        );

        $validator_arr = array(
            'partner_id' => 'required|integer',
            'operacija_id' => 'required|integer',
            'cena' => 'required|numeric|digits_between:1,15',
        );

        $validator = Validator::make($data, $validator_arr, $messages);
        if ($validator->passes()) {

        	if($data['operacija_servis_id'] == 0){    		
	        	if(DB::table('operacija_servis')->where(array('partner_id'=>$data['partner_id'],'operacija_id'=>$data['operacija_id']))->count() > 0){
	        		return Response::json(array('success'=>false,'message'=>'Cena za ovog partnera već postoji.'));
	        	}

				$operacija_servis_id = DB::table('operacija_servis')->max('operacija_servis_id') + 1;
				DB::table('operacija_servis')->insert(
					array(
						'operacija_servis_id'=> $operacija_servis_id,
						'partner_id'=>$data['partner_id'],
						'operacija_id'=>$data['operacija_id'],
						'tarifna_grupa_id'=> 0,
						'cena'=>$data['cena']
					)
				);
        	}else{
        		DB::table('operacija_servis')->where('operacija_servis_id',$data['operacija_servis_id'])->update(array('cena'=>$data['cena']));
        	}
			return Response::json(array('success'=>true,'message'=>'Uspešno ste sačuvali vrednost.'));
		}else{
			return Response::json(array('success'=>false,'message'=>$validator->messages()->first()));
		}
	}

	public function operacije_export(){
		$results = DB::table('operacija')->select('operacija_grupa.naziv as operacija_grupa_naziv','operacija.naziv as operacija_naziv','cena')->leftJoin('operacija_grupa','operacija_grupa.operacija_grupa_id','=','operacija.operacija_grupa_id')->orderBy('operacija_grupa.naziv','asc')->orderBy('operacija.naziv','asc')->get();

		$data = array(
					array('Naziv grupe operacija','Naziv operacije','cena')
				);
		foreach($results as $item){
			$data[] = array(
				$item->operacija_grupa_naziv,
				$item->operacija_naziv,
				$item->cena
			);
		}
		
		$doc = new PHPExcel();
		$doc->setActiveSheetIndex(0);
		$doc->getActiveSheet()->fromArray($data);
		// $doc->createSheet();
		// $doc->setActiveSheetIndex(1);
		// $doc->getActiveSheet()->fromArray($itemData);

		// header('Content-Type: application/vnd.ms-excel');
		// header('Content-Disposition: attachment;filename="radne_operacije_'.date('Y-m-d').'.xls"');
		// header('Cache-Control: max-age=0');
		 
		$objWriter = PHPExcel_IOFactory::createWriter($doc, 'Excel5');
		 
		// $objWriter->save('php://output');
		$store_path = 'files/radne_operacije_'.date('Y-m-d').'.xls';
		$objWriter->save($store_path);
		return Redirect::to(RmaOptions::base_url().$store_path);

        // header("Content-Type: application/vnd.ms-excel");
        // header("Content-disposition: attachment; filename=radne_operacije_".date('Y-m-d').".xls");

        // echo 'Naziv grupe operacija'."\t" . 'Naziv operacije'."\t" . 'Cena'."\t\n";

        // $operacije_all = "";
        // foreach($results as $item)
        // {        
        //     $operacije_all .= RmaSupport::clear_special_chars($item->operacija_grupa_naziv) ."\t". RmaSupport::clear_special_chars($item->operacija_naziv) ."\t". $item->cena ."\t\n";
        // }
        // echo $operacije_all;
	}

}