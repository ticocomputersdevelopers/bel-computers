<?php
namespace IsMinimax;



class ApiData {

	public static function getToken(){
	
		$params = array(
	        'client_id'=> 'Aquatec',
	        'client_secret'=> 'Aquatec',
	        'grant_type'=> 'password',
	        'username'=> 'racunovodstvoaquatec@gmail.com',
	        'password'=> 'aqu123',
	        'scope' => 'minimax.rs'
	    );
	    $request = array(
	        'http' => array(
	            'method'=> 'POST',
	            'header'=> array(
	                'Content-type: application/x-www-form-urlencoded'
	            ),
	            'content'=> http_build_query($params),
	            'timeout'=> 100000
	        )
	    );
	    if(!$response = file_get_contents('https://moj.minimax.rs/RS/aut/oauth20/token', false, stream_context_create($request))) {
	        return null;
	    }
	    $token = json_decode($response);
	    return $token;
	}

	public static function articles($token,$page){
		
		$request = array(
	        'http' => array(
	            'method'=> 'GET',
	            'header'=> array(
	                'Authorization: Bearer ' . $token->access_token,
	            ),
	            'timeout'=> 10000
	        )
	    );	    
	    if (!$response = file_get_contents('https://moj.minimax.rs/RS/api/api/orgs/1877/items?SortField=Code&Order=A&PageSize=100&CurrentPage='.$page, false, stream_context_create($request))) {
	        die('error');
	    }
	    $results = json_decode($response, true);
	    return $results;
	}

	public static function stock($token,$page){
		
		$request = array(
	        'http' => array(
	            'method'=> 'GET',
	            'header'=> array(
	                'Authorization: Bearer ' . $token->access_token,
	            ),
	            'timeout'=> 10000
	        )
	    );	    
	    if (!$response = file_get_contents('https://moj.minimax.rs/RS/api/api/orgs/1877/stocks?SortField=ItemCode&Order=A&PageSize=100&CurrentPage='.$page, false, stream_context_create($request))) {
	        die('error');
	    }
	    $results = json_decode($response, true);
	    return $results;
	}
	
	public static function partners($token,$page,$partner_details=false){
		
		$request = array(
	        'http' => array(
	            'method'=> 'GET',
	            'header'=> array(
	                'Authorization: Bearer ' . $token->access_token,
	            ),
	            'timeout'=> 10000
	        )
	    );	    
	    if (!$response = file_get_contents('https://moj.minimax.rs/RS/api/api/orgs/1877/customers?SortField=CustomerId&Order=A&PageSize=100&CurrentPage='.$page, false, stream_context_create($request))) {
	        die('error');
	    }
	    $results = json_decode($response, true);
	    if($partner_details){
	    	foreach ($results['Rows'] as $key => $partner) {
	    	
	    		$results['Rows'][$key]['PartnerDetails'] = self::partner($token,$partner['CustomerId']);
	    	}
		}
	    return $results;
	}
	public static function partner($token,$customerId){
		
		$request = array(
	        'http' => array(
	            'method'=> 'GET',
	            'header'=> array(
	                'Authorization: Bearer ' . $token->access_token,
	            ),
	            'timeout'=> 10000
	        )
	    );	    
	    if (!$response = file_get_contents('https://moj.minimax.rs/RS/api/api/orgs/1877/customers/'.$customerId, false, stream_context_create($request))) {
	        die('error');
	    }
	    $results = json_decode($response, true);
	    return $results;
	}
}