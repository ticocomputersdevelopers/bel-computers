<?php
ini_set('memory_limit', '-1');
ini_set('max_execution_time', 3600);

use IsRoaming\FileData;
use IsRoaming\Article;
use IsRoaming\ArticlePartnerGroup;
use IsRoaming\Stock;
use IsRoaming\Partner;
use IsRoaming\PartnerCard;
use IsRoaming\PartnerBill;
use IsRoaming\PartnerBillItem;
use IsRoaming\Seller;
use IsRoaming\PartnerSeller;
use IsRoaming\Address;
use IsRoaming\Support;


class AdminIsRoaming {

    public static function execute(){

        try {
            $partners = FileData::partners();
            $articleDetails = FileData::articles();
            $articlesPricesDetails = FileData::articles_prices();
            $partnersCards = FileData::partners_cards();
            $partnersCardItems = FileData::partners_card_items();
            $address = FileData::address();


            //partner
            $resultPartner = Partner::table_body($partners);
            Partner::query_insert_update($resultPartner->body);
            $mappedPartners = Support::getMappedPartners();

            //seller
            $sellers = Support::getSellers($partners);
            $resultSeller = Seller::table_body($sellers);
            Seller::query_insert_update($resultSeller->body);
            $mappedSellers = Support::getMappedSellers();

            //partner seller
            $resultPartnerSeller = PartnerSeller::table_body($partners,$mappedSellers,$mappedPartners);
            PartnerSeller::query_insert_update($resultPartnerSeller->body);


            //articles
            $resultArticle = Article::table_body($articleDetails->articles);
            Article::query_insert_update($resultArticle->body,array('barkod','racunska_cena_nc','racunska_cena_end','mpcena','proizvodjac_id'));
            Article::query_update_unexists($resultArticle->body);
            $mappedArticles = Support::getMappedArticles();

            //articles prices
            $articlePartnerGroup = ArticlePartnerGroup::table_body($articlesPricesDetails->articles_prices,$mappedArticles);
            ArticlePartnerGroup::query_insert_update($articlePartnerGroup->body);
            // ArticlePartnerGroup::query_delete_unexists($articlePartnerGroup->body);

            //b2b stock
            if(AdminB2BOptions::info_sys('roaming')->b2b_magacin){
                $resultStock = Stock::table_body($articleDetails->articles);
                Stock::query_insert_update($resultStock->body);
                Stock::query_update_unexists($resultStock->body);
            }

            //partner card
            $resultPartnersCards = PartnerCard::table_body($partnersCards,$mappedPartners);
            PartnerCard::query_insert_update($resultPartnersCards->body);

            //partner bill
            $resultPartnersBills = PartnerBill::table_body($partnersCards,$mappedPartners);
            PartnerBill::query_insert_update($resultPartnersBills->body);
            $getMappedBills = Support::getMappedBills();

            //partner bill items
            $resultPartnersBillItems = PartnerBillItem::table_body($partnersCardItems,$getMappedBills,$mappedArticles);
            PartnerBillItem::query_insert_update($resultPartnersBillItems->body);

            //address
            $resultAddress = Address::table_body($address,$mappedPartners);
            Address::query_insert_update($resultAddress->body);


            AdminB2BIS::saveISLog('true');
            return (object) array('success'=>true);
        }catch (Exception $e){
            AdminB2BIS::saveISLog('false');
            return (object) array('success'=>false,'message'=>$e->getMessage());
        }
    }



}