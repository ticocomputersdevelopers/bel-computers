<?php
namespace Import;
use Import\Support;
use DB;
use AdminOptions;
use File;
use ZipArchive;

class Velteh {

	public static function execute($dobavljac_id,$extension=null){
		
		Support::initQueryExecute();

		if($extension==null){
			if(Support::autoLink($dobavljac_id) != null && Support::serviceUsername($dobavljac_id) != null && Support::servicePassword($dobavljac_id) != null){
				self::autoDownload(Support::autoLink($dobavljac_id),Support::serviceUsername($dobavljac_id),Support::servicePassword($dobavljac_id));
			}
			$file_name = Support::file_name("files/velteh/velteh_xml/");
			if($file_name!==false){
				$products = simplexml_load_file("files/velteh/velteh_xml/".$file_name);
			}else{
				$products = array();
			}
		}else{
			$file = AdminOptions::base_url().'files/import.'.$extension;
			$products = simplexml_load_file($file);	
		}

		//podaci o proizvodima
		foreach ($products as $product) {
			$insert_arr = array();
			$insert_arr['partner_id'] = $dobavljac_id;
			$insert_arr['sifra_kod_dobavljaca'] = $product->sifra;
			$insert_arr['naziv'] = Support::encodeTo1250(pg_escape_string($product->naziv));
			$insert_arr['grupa'] = Support::encodeTo1250($product->kategorija);			
			$insert_arr['cena_nc'] = $product->cena_bez_pdv;
			$insert_arr['kolicina'] = "1";
			$insert_arr['flag_opis_postoji'] = $product->opis == "" ? "0" : "1";
			$insert_arr['opis'] = Support::encodeTo1250(pg_escape_string($product->opis));
			$insert_arr['web_flag_karakteristike'] = "0";
			$insert_arr['flag_slika_postoji'] = $product->link == "" ? "0" : "1";

			DB::table('dobavljac_cenovnik_temp')->insert($insert_arr);

			if($insert_arr['flag_slika_postoji'] == '1'){
				$sPolja = '';
				$sVrednosti = '';
				$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
				$sPolja .= "sifra_kod_dobavljaca,";		$sVrednosti .= "" . Support::quotedStr($product->sifra) . ",";
				$sPolja .= "akcija,";		            $sVrednosti .= "" . 1 . ",";
				$sPolja .= "putanja";				    $sVrednosti .= "'" . $product->link. "'";
				
					
				DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");
			}
		}

		Support::queryExecute($dobavljac_id,array('i','u'),array('i'),array('i','u'));

		//Brisemo fajl
		$file_name = Support::file_name("files/velteh/velteh_xml/");
		if($extension!=null){
			File::delete('files/import.'.$extension);
		}
		elseif($file_name!==false){
			File::delete("files/velteh/velteh_xml/".$file_name);
		}

	}

	public static function executeShort($dobavljac_id,$kurs=null,$extension=null){
		Support::initQueryExecute();

		if($extension==null){
			if(Support::autoLink($dobavljac_id) != null && Support::serviceUsername($dobavljac_id) != null && Support::servicePassword($dobavljac_id) != null){
				self::autoDownload(Support::autoLink($dobavljac_id),Support::serviceUsername($dobavljac_id),Support::servicePassword($dobavljac_id));
			}
			$file_name = Support::file_name("files/velteh/velteh_xml/");
			if($file_name!==false){
				$products = simplexml_load_file("files/velteh/velteh_xml/".$file_name);
			}else{
				$products = array();
			}
		}else{
			$file = AdminOptions::base_url().'files/import.'.$extension;
			$products = simplexml_load_file($file);			
		}

		//podaci o proizvodima
		foreach ($products as $product) {
			$insert_arr = array();
			$insert_arr['partner_id'] = $dobavljac_id;
			$insert_arr['sifra_kod_dobavljaca'] = $product->attributes()->code;
			$insert_arr['kolicina'] = $product->onStock == "true" ? "1" : "0";
			$insert_arr['cena_nc'] = $product->priceA;
			$insert_arr['mpcena'] = $product->priceC;
			$insert_arr['pmp_cena'] = $product->priceC;

			DB::table('dobavljac_cenovnik_temp')->insert($insert_arr);
		}

		//Support::queryShortExecute($dobavljac_id);

		//Brisemo fajl
		$file_name = Support::file_name("files/velteh/velteh_xml/");
		if($extension!=null){
			File::delete('files/import.'.$extension);
		}
		elseif($file_name!==false){
			File::delete("files/velteh/velteh_xml/".$file_name);
		}

	}

	public static function autoDownload($links,$username,$password){

		$links = $links;
		$postinfo = "j_username=".$username."&j_password=".$password."";
		$cookie_file_path = 'cookie.txt';

		$myfile = fopen("files/velteh/velteh_xml/velteh.zip", "w");

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_NOBODY, false);
		curl_setopt($ch, CURLOPT_URL, $links[0]);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);

		curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_file_path);
		curl_setopt($ch, CURLOPT_COOKIE, "cookiename=0");
		curl_setopt($ch, CURLOPT_USERAGENT,
		    "Mozilla/5.0 (Windows; U; Windows NT 5.0; en-US; rv:1.7.12) Gecko/20050915 Firefox/1.0.7");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_REFERER, $_SERVER['REQUEST_URI']);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);

		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postinfo);
		curl_exec($ch);

		//dovlacenje xml-a
		curl_setopt($ch, CURLOPT_URL, $links[1]);
		curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
		curl_setopt($ch, CURLOPT_VERBOSE, 1);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
		curl_setopt($ch, CURLOPT_FILE, $myfile);
		curl_exec ($ch);
		curl_close($ch);

		$zip = new ZipArchive;
		if ($zip->open('files/velteh/velteh_xml/velteh.zip') === TRUE) {
		    $zip->extractTo('files/velteh/velteh_xml');
		    $zip->close();
		    File::delete('files/velteh/velteh_xml/velteh.zip');
		}
	}	

}

?>